#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
using namespace cv;
using namespace std;
double maxOdlegloscSrodkow = 70;
bool wyswEfektPosredni = true;
bool wyswEfektKoncowy = true;
bool wyswOryginalny = false;
bool delta(RotatedRect a, RotatedRect b) {
	return norm(a.center - b.center) <= maxOdlegloscSrodkow;
}
void znajdzWszystkieElipsy(int liczbaIteracji, Mat obrazPrzetwarzany, Mat& obrazWczytany,
		int silaMedianBlur, int progTresholdMin, int progTresholdMax,
		int maxWartoscBinarna, int typProgowania, int ktoryTresholdPokazac,
		vector<vector<Point> >& kontury,
		const int minRozmiarAkceptowanychKontur, Mat& obrazWyswietlany,
		vector<vector<RotatedRect> >& elipsy) {
	for (int i = 0; i < liczbaIteracji; i++) {
		obrazPrzetwarzany = obrazWczytany.clone();
		medianBlur(obrazWczytany, obrazPrzetwarzany, silaMedianBlur);
		double progTreshold = progTresholdMin
				+ (((double) (i)) / ((double) (liczbaIteracji)))
						* (double) (((progTresholdMax - progTresholdMin)));
		threshold(obrazPrzetwarzany, obrazPrzetwarzany, progTreshold, maxWartoscBinarna,
				typProgowania);
		if (i == ktoryTresholdPokazac) {
			obrazWyswietlany = obrazPrzetwarzany.clone();
			cvtColor(obrazWyswietlany, obrazWyswietlany, CV_GRAY2BGR);
		}
		vector<Vec4i> hierarchy;
		findContours(obrazPrzetwarzany, kontury, hierarchy, CV_RETR_TREE,
				CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		if (i == ktoryTresholdPokazac) {
			for (auto j = 0u; j < kontury.size(); j++) {
				Scalar color = Scalar(0, 255, 0);
				drawContours(obrazWyswietlany, kontury, j, color, 2, 8,
						hierarchy, 0, Point());
			}
		}
		cout << "Liczba kontur: " << kontury.size() << endl;
		elipsy.push_back(vector<RotatedRect>(kontury.size()));
		for (auto j = 0u; j < kontury.size(); j++)
			if (kontury[j].size() > static_cast<unsigned int>(minRozmiarAkceptowanychKontur))
				(elipsy.back()).push_back(fitEllipse(kontury[j]));
		cout << "Liczba elips: " << elipsy.size() << endl;
	}
}
void rysujProstokatyOpisaneNaWszystkichElipsach(
		const vector<vector<RotatedRect> >& elipsy, Point2f wierzcholki[4],
		const Mat& obrazWyswietlany, int gruboscLinii, int typLinii) {
	for (auto ev : elipsy)
		for (auto e : ev) {
			e.points(wierzcholki);
			for (int i = 0; i < 4; i++)
				line(obrazWyswietlany, wierzcholki[i], wierzcholki[(i + 1) % 4],
						Scalar(0, 0, 255), gruboscLinii, typLinii);
		}
}
void warunkowoWyswietlEfektPosredni(Mat& obrazWyswietlany, char** argv) {
	if (wyswEfektPosredni) {
		resize(obrazWyswietlany, obrazWyswietlany, Size(), 0.1, 0.1,
				INTER_LINEAR);
		namedWindow(String(argv[1]) + " Efekt", WINDOW_AUTOSIZE);
		imshow(String(argv[1]) + " Efekt", obrazWyswietlany);
	}
}
void wypiszWszystkieElipsy(const vector<vector<RotatedRect> >& elipsy) {
	cout << "Wszystkie elipsy:" << endl;
	for (auto ev : elipsy) {
		for (auto e : ev)
			cout << "Center: " << e.center << ", Kąt: " << e.angle << " ";
		cout << endl;
	}
}
void odfiltrujIPodzielZbiorElipsNaZbioryZElipsamiOBliskichSrodkach(
		const vector<vector<RotatedRect> >& elipsy,
		const int minWysAkceptowanychElips, const int minSzerAkceptowanychElips,
		const int maxWysAkceptowanychElips, const int maxSzerAkceptowanychElips,
		vector<vector<RotatedRect> >& elipsy_podzielone) {
	for (auto ev : elipsy) {
		for (auto e : ev) {
			if (e.size.height > 0 && e.size.width > 0
					&& e.size.height > minWysAkceptowanychElips
					&& e.size.width > minSzerAkceptowanychElips
					&& e.size.height < maxWysAkceptowanychElips
					&& e.size.width < maxSzerAkceptowanychElips) {
				bool znaleziono = false;
				for (auto& evp : elipsy_podzielone) {
					if (evp.size() > 0 && delta(evp.back(), e)) {
						znaleziono = true;
						evp.push_back(e);
						break;
					} //cout << "Done" << endl;
				}
				if (!znaleziono) {
					elipsy_podzielone.push_back(vector<RotatedRect>(1));
					elipsy_podzielone.back().push_back(e);
				}
			}
		}
	}
}
void znajdzNajliczniejszyZbiorElips(
		const vector<vector<RotatedRect> >& elipsy_podzielone, int maxRozmiar,
		int najliczniejszyZbior, const Mat& obrazWczytany) {
	for (unsigned int i = 0u; i < elipsy_podzielone.size(); i++) {
		if (maxRozmiar < (int) (elipsy_podzielone[i].size())) {
			najliczniejszyZbior = i;
			maxRozmiar = elipsy_podzielone[i].size();
		}
	}
	cout << "Srodekx: " << obrazWczytany.cols / 2;
	cout << "Srodeky: " << obrazWczytany.rows / 2;
}
bool porownajOdleglosciProstokatowOdSrodka(const vector<RotatedRect>& a,
		const vector<RotatedRect>& b, const Mat& obrazWczytany) {
	float a_avgx = 0, a_avgy = 0, b_avgx = 0, b_avgy = 0;
	for (auto v : a) {
		a_avgx += v.center.x;
		a_avgy += v.center.y;
	}
	a_avgx = a.size() == 0 ? 1.0 : a_avgx / (float) (a.size());
	a_avgy = a.size() == 0 ? 1.0 : a_avgy / (float) (a.size());
	for (auto v : b) {
		b_avgx += v.center.x;
		b_avgy += v.center.y;
	}
	b_avgx = b.size() == 0 ? 1.0 : b_avgx / (float) (b.size());
	b_avgy = b.size() == 0 ? 1.0 : b_avgy / (float) (b.size());
	float odlegloscOdSrodkaAx = a_avgx - obrazWczytany.cols / 2;
	float odlegloscOdSrodkaAy = a_avgy - obrazWczytany.rows / 2;
	float odlegloscOdSrodkaA = odlegloscOdSrodkaAx * odlegloscOdSrodkaAx
			+ odlegloscOdSrodkaAy * odlegloscOdSrodkaAy;
	odlegloscOdSrodkaA = odlegloscOdSrodkaA == 0 ? 1.0 : odlegloscOdSrodkaA;
	float odlegloscOdSrodkaBx = b_avgx - obrazWczytany.cols / 2;
	float odlegloscOdSrodkaBy = b_avgy - obrazWczytany.rows / 2;
	float odlegloscOdSrodkaB = odlegloscOdSrodkaBx * odlegloscOdSrodkaBx
			+ odlegloscOdSrodkaBy * odlegloscOdSrodkaBy;
	odlegloscOdSrodkaB = odlegloscOdSrodkaB == 0 ? 1.0 : odlegloscOdSrodkaB;
	return odlegloscOdSrodkaA < odlegloscOdSrodkaB;
}
void wypiszUsrednioneOdleglosciOdSrodkaWszystkichZbiorowElips(
		const Mat& obrazWczytany,
		vector<vector<RotatedRect> >& elipsy_podzielone) {
	std::for_each(elipsy_podzielone.begin(), elipsy_podzielone.end(),
			[obrazWczytany](vector<RotatedRect> a) {
				float a_avgx = 0, a_avgy = 0;
				for (auto v: a) {
					a_avgx += v.center.x;
					a_avgy += v.center.y;
				}
				a_avgx = a.size() == 0 ? 1.0 : a_avgx / (float)(a.size());
				a_avgy = a.size() == 0 ? 1.0 : a_avgy / (float)(a.size());
				float odlegloscOdSrodkaAx = a_avgx - obrazWczytany.cols / 2;
				float odlegloscOdSrodkaAy = a_avgy - obrazWczytany.rows / 2;
				float odlegloscOdSrodkaA = odlegloscOdSrodkaAx * odlegloscOdSrodkaAx + odlegloscOdSrodkaAy * odlegloscOdSrodkaAy;
				odlegloscOdSrodkaA = odlegloscOdSrodkaA == 0 ? 1.0 : odlegloscOdSrodkaA;
				cout << " a_avgx: " << a_avgx;
				cout << " a_avgy: " << a_avgy;
				cout << " odlegloscOdSrodkaAx: " << odlegloscOdSrodkaAx;
				cout << " odlegloscOdSrodkaAy: " << odlegloscOdSrodkaAy;
				cout << " Odleglosc od srodka: " << odlegloscOdSrodkaA << endl;
			});
}
void wybierzNajwiekszaElipseZNajliczniejszegoZbioruJakoNajlepsza(
		const vector<vector<RotatedRect> >& elipsy_podzielone,
		RotatedRect& najlepszaElipsa) {
	for (auto ep : elipsy_podzielone[0]) {
		if (najlepszaElipsa.size.height + najlepszaElipsa.size.width
				< ep.size.height + ep.size.width)
			najlepszaElipsa = ep;
	}
}
void sprawdzCzyJestLepszeRozwiazanieWsrodWszystkichElips(
		const vector<vector<RotatedRect> >& elipsy,
		const int minWysAkceptowanychElips, const int minSzerAkceptowanychElips,
		const int maxWysAkceptowanychElips, const int maxSzerAkceptowanychElips,
		const RotatedRect& srodek, RotatedRect& najlepszaElipsa) {
	for (auto ev : elipsy) {
		for (auto e : ev) {
			if (e.size.height > 0 && e.size.width > 0
					&& e.size.height > minWysAkceptowanychElips
					&& e.size.width > minSzerAkceptowanychElips
					&& e.size.height < maxWysAkceptowanychElips
					&& e.size.width < maxSzerAkceptowanychElips
					&& delta(e, srodek))
				najlepszaElipsa = e;
		}
	}
}
void wypiszSrodek(const Mat& obrazWczytany) {
	cout << "Srodekx: " << obrazWczytany.cols / 2;
	cout << "Srodeky: " << obrazWczytany.rows / 2;
}
void wypiszNajlepszaElipse(const RotatedRect& najlepszaElipsa) {
	cout << "Najlepsza elipsa:" << endl;
	cout << "Center: " << najlepszaElipsa.center << ", Kąt: "
			<< najlepszaElipsa.angle << ", Size: " << najlepszaElipsa.size
			<< " " << endl;
}
void warunkowoWyswietlEfektKoncowy(Mat obrazWyswietlany,
		const Mat& obrazWczytany, const RotatedRect& najlepszaElipsa,
		Point2f wierzcholki[4], int gruboscLinii, int typLinii, char** argv) {
	obrazWyswietlany = obrazWczytany;
	najlepszaElipsa.points(wierzcholki);
	for (int i = 0; i < 4; i++)
		line(obrazWyswietlany, wierzcholki[i], wierzcholki[(i + 1) % 4],
				Scalar(0, 0, 255), gruboscLinii, typLinii);
	if (wyswEfektKoncowy) {
		resize(obrazWyswietlany, obrazWyswietlany, Size(), 0.1, 0.1,
				INTER_LINEAR);
		namedWindow(String(argv[1]) + " Efekt Końcowy", WINDOW_AUTOSIZE);
		imshow(String(argv[1]) + " Efekt Końcowy", obrazWyswietlany);
	}
}
void warunkowoWyswietlOryginalnyObraz(const Mat& obrazWczytany,
		const Mat& obrazWyswietlany, char** argv) {
	if (wyswOryginalny) {
		resize(obrazWczytany, obrazWyswietlany, Size(), 0.2, 0.2, INTER_LINEAR);
		imshow(String(argv[1]) + " OryginalnyObrazOka", obrazWyswietlany);
	}
}
void normalizujObrazOrazKonwertujDoCV8UC1(Mat& obrazPrzetwarzany,
		Mat& obrazWczytany) {
	double minWartosc, maksWartosc;
	minMaxLoc(obrazPrzetwarzany, &minWartosc, &maksWartosc);
	obrazPrzetwarzany.convertTo(obrazWczytany, CV_8UC1,
			255.0 / (maksWartosc - minWartosc),
			-minWartosc * 255.0 / (maksWartosc - minWartosc));
}
void wyzerujNiebieskiOrazCzerwonyKanal(Mat& obrazWczytany,
		Mat kanaly[3]) {
	split(obrazWczytany, kanaly);
	kanaly[0] = Mat::zeros(obrazWczytany.rows, obrazWczytany.cols, CV_8UC1);
	kanaly[1] = Mat::zeros(obrazWczytany.rows, obrazWczytany.cols, CV_8UC1);
	merge(kanaly, 3, obrazWczytany);
}
int main(int argc, char** argv) {
	//np. Retinopathy ObrazWnetrzaOka 3 100 230 255 1 30 15 40 120 120 450 450 70 1 1 0
	const int l_liczbaParametrow = 18;
	if (argc != l_liczbaParametrow) {
		cout
				<< " Uzycie: Retinopathy ObrazWnetrzaOka silaMedianBlur progTresholdMin progTresholdMax maxWartoscBinarna typTreshold liczbaIteracji ktoryTresholdPokazac minRozmiarAkceptowanychKontur minWysAkceptowanychElips minSzerAkceptowanychElips maxWysAkceptowanychElips maxSzerAkceptowanychElips maxOdlegloscSrodkow wyswEfektPosredni wyswEfektKoncowy wyswOryginalny"
				<< endl;
		return -1;
	}
	//parametry:
	int silaMedianBlur = std::stoi(argv[2], nullptr);
	int progTresholdMin = std::stoi(argv[3], nullptr);
	int progTresholdMax = std::stoi(argv[4], nullptr);
	int maxWartoscBinarna = std::stoi(argv[5], nullptr);
	//int typTreshold = 3; //poprzednie
	int typTreshold = std::stoi(argv[6], nullptr); //binary inverted
	int liczbaIteracji = std::stoi(argv[7], nullptr);
	int ktoryTresholdPokazac = std::stoi(argv[8], nullptr); //normalnie bylo 0
	const int minRozmiarAkceptowanychKontur = std::stoi(argv[9], nullptr);
	const int minWysAkceptowanychElips = std::stoi(argv[10], nullptr);
	const int minSzerAkceptowanychElips = std::stoi(argv[11], nullptr);
	const int maxWysAkceptowanychElips = std::stoi(argv[12], nullptr);
	const int maxSzerAkceptowanychElips = std::stoi(argv[13], nullptr);
	maxOdlegloscSrodkow = std::stoi(argv[14], nullptr);
	wyswEfektPosredni = std::stoi(argv[15], nullptr);
	wyswEfektKoncowy = std::stoi(argv[16], nullptr);
	wyswOryginalny = std::stoi(argv[17], nullptr);
	Mat obrazWyswietlany;
	Mat obrazWczytany;
	obrazWczytany = imread(argv[1], CV_LOAD_IMAGE_COLOR);   // Przeczytaj plik
	if (!obrazWczytany.data)                        // Sprawdz poprawnosc danych
	{
		cout << "Nie udalo sie otworzyc lub znalezc obrazu" << std::endl;
		return -1;
	}
	Mat kanaly[3];
	warunkowoWyswietlOryginalnyObraz(obrazWczytany, obrazWyswietlany, argv);
	wyzerujNiebieskiOrazCzerwonyKanal(obrazWczytany, kanaly);
	Mat obrazPrzetwarzany;
	cvtColor(obrazWczytany, obrazPrzetwarzany, CV_BGR2GRAY);
	normalizujObrazOrazKonwertujDoCV8UC1(obrazPrzetwarzany, obrazWczytany);
	vector<vector<Point>> kontury;
	vector<vector<RotatedRect>> elipsy(liczbaIteracji);
	znajdzWszystkieElipsy(liczbaIteracji, obrazPrzetwarzany, obrazWczytany,
			silaMedianBlur, progTresholdMin, progTresholdMax, maxWartoscBinarna,
			typTreshold, ktoryTresholdPokazac, kontury,
			minRozmiarAkceptowanychKontur, obrazWyswietlany, elipsy);
	Point2f wierzcholki[4];
	int grubosc = 4;
	int typLinii = CV_AA;
	rysujProstokatyOpisaneNaWszystkichElipsach(elipsy, wierzcholki,
			obrazWyswietlany, grubosc, typLinii);
	warunkowoWyswietlEfektPosredni(obrazWyswietlany, argv);
	cout << "Wyniki:\n";
	wypiszWszystkieElipsy(elipsy);
	cout << "Obliczanie najlepszej elipsy:" << endl;
	vector<vector<RotatedRect>> elipsy_podzielone(liczbaIteracji);
	odfiltrujIPodzielZbiorElipsNaZbioryZElipsamiOBliskichSrodkach(elipsy,
			minWysAkceptowanychElips, minSzerAkceptowanychElips,
			maxWysAkceptowanychElips, maxSzerAkceptowanychElips,
			elipsy_podzielone);
	int najliczniejszyZbior = -1;
	int maxRozmiar = -1;
	znajdzNajliczniejszyZbiorElips(elipsy_podzielone, maxRozmiar,
			najliczniejszyZbior, obrazWczytany);
	std::sort(elipsy_podzielone.begin(), elipsy_podzielone.end(),
			[obrazWczytany](vector<RotatedRect> a, vector<RotatedRect> b)
			{ return porownajOdleglosciProstokatowOdSrodka(a, b, obrazWczytany);});
	wypiszUsrednioneOdleglosciOdSrodkaWszystkichZbiorowElips(obrazWczytany,
			elipsy_podzielone);
	RotatedRect najlepszaElipsa = elipsy_podzielone[0][0];
	wybierzNajwiekszaElipseZNajliczniejszegoZbioruJakoNajlepsza(
			elipsy_podzielone, najlepszaElipsa);
	wypiszSrodek(obrazWczytany);
	RotatedRect srodek(Point2f(obrazWczytany.cols / 2, obrazWczytany.rows / 2),
			Size2f(1, 1), 1);
	sprawdzCzyJestLepszeRozwiazanieWsrodWszystkichElips(elipsy,
			minWysAkceptowanychElips, minSzerAkceptowanychElips,
			maxWysAkceptowanychElips, maxSzerAkceptowanychElips, srodek,
			najlepszaElipsa);
	wypiszNajlepszaElipse(najlepszaElipsa);
	warunkowoWyswietlEfektKoncowy(obrazWyswietlany, obrazWczytany,
			najlepszaElipsa, wierzcholki, grubosc, typLinii, argv);
	waitKey(0);
	return 0;
}